# OEE Search Driver #

This driver provides a simplified access OEE Search's backend, built on socket.io

## How do I get set up? ##

The HTML file implementing this driver must include the following files:-
- index.js ([available in this repository](./index.js))
- https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.1/socket.io.dev.min.js

The socket.io driver has to added to in the HTML files, since socket.io has not module export from within.

[Readmore about this for socket.io](https://stackoverflow.com/questions/52310242/socket-io-client-no-default-export)


To add the driver and socket.io
```html
<script src="/index.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.1/socket.io.dev.min.js"></script>
```

## Integration ##

- ### add socket.io file
`<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.1/socket.io.dev.min.js"></script>`

- ### add driver file
`<script src="https://bitbucket.org/rishavbhowmiktgs/oee-search-driver/raw/ef4f0b73eb0289d8dfb7ff91b7a545039ac2a2e7/index.js"></script>`

*You can also copy this js from this repository*

- ### Use OeeSearch Class

- OeeSearch Class is the main component of the driver's JS file
```html
<script>
    const search_driver = new OeeSearch('http://localhost:8393', '<user_id>', '<org_id>', (data)=>{
        /*
            This the on_result callback function, this function is triggered when the oee_search server return some data as search result
        
            Schema of 'data' is available in RESULT_DATA_SCHEMA.md
        */
    })
    
    search_driver.connect(io) //io orignates from socket.io files and must be passed as it is
    .then(()=>{
        //when connected
        //the server is ready to process the search queries
    })
    .catch((err)=>{
        //if error occurs while connecting
    })
</script>
```

- To send search query Use `search_driver.search` function

```javascript
const query = "machine 002"
search_driver.search(query)
```

- To update/change the on result callback function post calling the constructor, Use `search_driver.update_on_result` function

```javascript
search_driver.update_on_result((data)=>{
    // lets try something else with the callback
    // Schema of 'data' is available in RESULT_DATA_SCHEMA.md
})
```

*once new callback function is added, the previous callback function is removed*