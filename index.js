class OeeSearch{
    constructor(host_addr, use_id, org_id, on_result=()=>{}){
        if(typeof host_addr != 'string') throw Error("'host_addr' should be type string")
        if(typeof use_id != 'string')    throw Error("'use_id' should be type string")
        if(typeof org_id != 'string')    throw Error("'org_id' should be type string")
        if(typeof on_result != 'function')    throw Error("'org_id' should be type function")
        this.host_addr = host_addr
        this.use_id = use_id
        this.org_id = org_id
        this.socket = null
        this.on_result = on_result
    }

    //must be called before using funtion query
    connect(io){
        const this_class = this
        return new Promise((resolve, reject)=>{
            this_class.socket = io(this_class.host_addr, {transports: ['websocket']})
            this_class.socket.on('session_ready', _ => { //3
                console.log('redy');
                //initializing search operation features
                this_class.search = (query)=>{
                    this_class.#__search(query)
                }
                this_class.search_worker = (query)=>{
                    this_class.#__search_worker(query)
                }
                resolve() //4
            })
            this_class.socket.on('start_session', data => { //2
                this_class.socket.emit('start_session',
                    {user_id:this_class.use_id, organisation_id:this_class.org_id}
                ) //1
            })

            this_class.socket.on('results', data =>{
                //console.log("Result", data);
                this_class.on_result(OeeSearch.process_results(data))
            })
            this_class.socket.on('results_worker_query', data => {
                if(typeof this_class.on_results_worker_query === 'function')
                    this_class.on_results_worker_query(data)
            })
        })
    }

    update_on_result(on_result){
        if(typeof on_result != 'function') throw "on_result should be type function"
        this.on_result = on_result
    }

    update_on_worker_result(on_result){
        if(typeof on_result != 'function') throw "on_result should be type function"
        this.on_results_worker_query = on_result
    }

    #__search(query){
        this.socket.emit('query', {query:query})
        //console.log("sent");
    }

    #__search_worker(query){
        this.socket.emit('worker_query', {query})
    }

    load_machine(machine_id){
        const this_class = this
        return new Promise((resolve, reject)=>{
            this_class.socket.emit('load_machine', { machine_id , req_key:0})
            var done = 0;
            this_class.socket.on('load_machine_result', (data)=>{
                if(data.machine_id == machine_id){
                    resolve()
                    done = 1;

                    setTimeout(()=>{this.socket.removeListener('load_machine_result')}, 5)

                    //for issues
                    this_class.search_issues = (query)=>{
                        console.log(query);
                        this_class.#__search_issues(query)
                    }
                    this_class.on_results_issues_query = (data)=>{}
                    this_class.socket.on('results_issues_query', (data)=>{
                        this_class.on_results_issues_query(data)
                    })

                    //for pps
                    this_class.search_pp = (query)=>{
                        this_class.#__search_pp(query)
                    }
                    this_class.on_results_pp_query = (data)=>{}
                    this_class.socket.on('results_pp_query', (data)=>{
                        this_class.on_results_pp_query(data)
                    })

                    //for dtps
                    this_class.search_downtimeplan = (query)=>{
                        this_class.#__search_downtimeplan(query)
                    }
                    this_class.on_results_downtimeplan_query = (data)=>{}
                    this_class.socket.on('results_planedDowntime_query', (data)=>{
                        this_class.on_results_downtimeplan_query(data)
                    })
                }
            })
            setTimeout(()=>{if(done===0) reject();done = -1; this.socket.removeListener('load_machine_result')}, 10000)
        })
    }

    update_on_issues_result(on_result){
        if(typeof on_result != 'function') throw "on_result should be type function"
        this.on_results_issues_query = on_result
    }

    update_on_pp_result(on_result){
        if(typeof on_result != 'function') throw "on_result should be type function"
        this.on_results_pp_query = on_result
    }

    update_on_downtimeplan_results(on_result){
        if(typeof on_result != 'function') throw "on_result should be type function"
        this.on_results_downtimeplan_query = on_result
    }

    #__search_issues(query){
        this.socket.emit('issues_query', {query})
        console.log("sent"+query);
    }
    #__search_pp(query){
        this.socket.emit('pp_query', {query})
    }
    #__search_downtimeplan(query){
        this.socket.emit('planedDowntime_query', {query})
    }

    static process_results(data){
        data.__org.sort((a,b)=>{
            b.score - a.score
        })
        data.__tg.sort((a,b)=>{
            b.score - a.score
        })
        data.__panel.sort((a,b)=>{
            b.score - a.score
        })
        return (data)
    }
}