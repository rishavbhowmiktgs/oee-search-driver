const http = require('http');
const fs = require('fs/promises')

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    if(req.url == '/index.js'){
        res.setHeader('Content-Type', 'text/javascript')
        fs.readFile('./index.js', {encoding:'utf8'})
        .then((file)=>{
            res.end(file)
        })
    }
    else{
        res.setHeader('Content-Type', 'text/html')
        fs.readFile('./test.html', {encoding:'utf8'})
        .then((file)=>{
            res.end(file)
        })
    }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});