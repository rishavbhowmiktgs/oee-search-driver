### Schema
    ```json
        {
            "type":"object",
            "properties": {
                "__org":{
                    "type":"array",
                    "properties":{
                        "score":{
                            "type":"number",
                            "min":1,
                            "description":"level of appropriteness of the result, Higher is better"
                        },
                        "result":{
                            "type":"object",
                            "properties":{
                                "display":{
                                    "type":"string",
                                    "description":"level of appropriteness of the result, Higher is better"
                                },
                               "url":{
                                    "type":"string",
                                    "description":"URL to direct the user to the respective location"
                                }
                            }
                        }
                    }
                },
                "__tg":{
                    "type":"array",
                    "properties":{
                        "score":{
                            "type":"number",
                            "min":1,
                            "description":"level of appropriteness of the result, Higher is better"
                        },
                        "result":{
                            "type":"object",
                            "properties":{
                                "display":{
                                    "type":"string",
                                    "description":"level of appropriteness of the result, Higher is better"
                                },
                               "url":{
                                    "type":"string",
                                    "description":"URL to direct the user to the respective location"
                                }
                            }
                        }
                    }
                },
                "__panel":{
                    "type":"array",
                    "properties":{
                        "score":{
                            "type":"number",
                            "min":1,
                            "description":"level of appropriteness of the result, Higher is better"
                        },
                        "result":{
                            "type":"object",
                            "properties":{
                                "display":{
                                    "type":"string",
                                    "description":"level of appropriteness of the result, Higher is better"
                                },
                               "url":{
                                    "type":"string",
                                    "description":"URL to direct the user to the respective location"
                                }
                            }
                        }
                    }
                }
            }
        }
    ```